#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    // QWidget interface
protected:
    void mouseMoveEvent(QMouseEvent *ev);
private slots:
    void on_pbMenu_clicked();
    void on_pbEDMT_clicked();
};

#endif // MAINWINDOW_HPP
