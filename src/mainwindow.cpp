#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include <QMouseEvent>
#include <QMenu>
#include <QTextStream>
#include <QMessageBox>
#include <QDesktopWidget>
#include <QApplication>
#include <windows.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::mouseMoveEvent(QMouseEvent *ev)
{
    ui->lbClientCoords->setText(QString("%1 %2").arg(ev->x()).arg(ev->y()));
    ui->lbScreenCoords->setText(QString("%1 %2").arg(ev->screenPos().x()).arg(ev->screenPos().y()));
    ev->ignore();
}

void MainWindow::on_pbMenu_clicked()
{
    QMenu m;
    m.addAction("Action 1");
    m.addAction("Action 2");
    QPoint pos=this->mapToGlobal(QPoint(0,0));
    int id=QApplication::desktop()->screenNumber(pos);
    QRectF s = QApplication::desktop()->screenGeometry(id);
    QString str;
    {
        QTextStream ts(&str);
        ts<<pos.x()<<" "<<pos.y()<<"\n"<<
            id<<"\n"<<
            "("<<s.left()<<","<<s.top()<<")-("<<s.right()<<","<<s.bottom()<<")";
        QList<QScreen *> screens = QApplication::screens();
        ts<<"\n"<<screens.size();
    }
    QMessageBox::information(this, "mapToGlobal", str);
    m.exec(pos);
}

BOOL CALLBACK MonitorEnumProc(
  _In_  HMONITOR hMonitor,
  _In_  HDC hdcMonitor,
  _In_  LPRECT lprcMonitor,
  _In_  LPARAM dwData
)
{
    QTextStream &ts=*(QTextStream *)(dwData);
    RECT &r=*lprcMonitor;
    ts<<"("<<r.left<<", "<<r.top<<")-("<<r.right<<", "<<r.bottom<<")\n";
    MONITORINFOEX mi;
    memset(&mi, 0, sizeof(mi));
    mi.cbSize=sizeof(mi);
    ts<<"GetMonitorInfo";
    if(GetMonitorInfo(hMonitor, &mi)==FALSE)
    {
        ts<<" failed\n";
    }
    else
    {
        ts<<" succeeded\n";
        ts<<QString::fromWCharArray(mi.szDevice)<<"\n";
    }
    ts<<"\n";
    return TRUE;
}

void MainWindow::on_pbEDMT_clicked()
{
    QString out;
    {
        QTextStream ts(&out);
        EnumDisplayMonitors(0, 0, &MonitorEnumProc, (LPARAM)&ts);
    }
    QMessageBox::information(this, "EnumDisplayMonitors", out);
}
